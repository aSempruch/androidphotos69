# Photos Android Project

Uploading Photos
---
Photos should be uploaded through Android Studio's `Device File Explorer` feature.
 > View > Tool Windows > Device File Explorer
 
Image files should be uploaded to `/storage/emulated/0/DCIM`

> If photos do not show up in the android file provider after pressing 'Add Photo',
> Make sure that SD Card setup has been competed (Android displays notification titled "SD Card Setup")

