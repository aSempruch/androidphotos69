package group69.photos69.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//import controller.UserViewController;

/**
 * User class read by program.
 * Holds lists of albums.
 * @author Andrew
 */
public class User implements Serializable{
    private static final long serialVersionUID = 1L;
    String username;
    List<Album> albumList;

    public User(String username) {
        this.username = username;
        albumList = new ArrayList<Album>();
        /*try {
            writeObject(this, "data/users", username);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }

    public String toString() {
        return username;
    }

    public int getAlbumCount() {
        return albumList.size();
    }

    public List<Album> getAlbumList(){
        return albumList;
    }

    /**
     * Renames the User and writes the object to disk.
     * @param newName Name to change to
     */
    public void renameUser(String newName) {
        this.username = newName;
        try {
            writeObject(this, "data/users", username);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Adds a new empty album to User's list with name
     * @param name Name of the new album
     * @return True/False of successful add
     */
    public boolean addNewAlbum(String name) {
        if(checkAlbumNameDuplicate(name)) {
            return false;
        }
        albumList.add(new Album(name, this));

        try {
            writeObject(this, "data/users", username);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Adds an album object to the User's list
     * @param a Album to Add
     * @return True/False of successful add
     */
    public boolean addNewAlbum(Album a) {
        if(checkAlbumNameDuplicate(a.getName())) {
            return false;
        }
        albumList.add(a);

        try {
            writeObject(this, "data/users", username);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Deletes an album object from the User's list
     * @param toDelete Album to delete
     * @return True/False of successful delete
     */
    public boolean removeAlbum(Album toDelete) {
        if(!albumList.remove(toDelete)) {
            return false;
        }
        try {
            writeObject(this, "data/users", username);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Rename's an album from User's list to newName
     * @param toRename Album being renamed
     * @param newName New name of the album
     * @return True/False of successful rename
     */
    public boolean renameAlbum(Album toRename, String newName) {
        int i = albumList.indexOf(toRename);
        if(i == -1) {
            return false;
        }
        if(checkAlbumNameDuplicate(newName)) {
            return false;
        }
        albumList.get(i).rename(newName);
        try {
            writeObject(this, "data/users", username);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Checks if an album of string name exists in User's list
     * @param name Name to check for
     * @return True/False album with name existing
     */
    private boolean checkAlbumNameDuplicate(String name) {
        for(Album a : albumList) {
            if(a.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Reads a serialized Album object
     * @param dir Directory to read from
     * @param filename Name of the file to read
     * @return The read album
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Album readObject(String dir, String filename) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(dir + File.separator + filename)
        );
        Album album = (Album)ois.readObject();
        ois.close();
        return album;
    }

    /**
     * Writes a serialized User object
     * @param user User to serialize
     * @param dir Directory to read from
     * @param filename of the file to write
     * @throws IOException
     */
    public static void writeObject(User user, String dir, String fileName) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(dir + File.separator + fileName)
        );
        oos.writeObject(user);
        oos.close();
    }
}

