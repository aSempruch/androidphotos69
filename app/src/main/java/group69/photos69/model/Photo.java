package group69.photos69.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.widget.ImageView;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.*;

/**
 * Photo object
 * @author Alan
 */
public class Photo implements Serializable {
    String caption;
    String location;
    Calendar date;
    HashMap<String, ArrayList<String>> tags;

    public Photo(String location) throws FileNotFoundException {

        /*if(!checkFile(location))
            throw new FileNotFoundException();*/

        String[] split = location.split("/");

        this.location = location;
        this.caption = split[split.length-1];
        this.tags = new HashMap<String, ArrayList<String>>();
        tags.put("People", new ArrayList<String>());
        this.date = getFileDate(location);
    }

    /**
     * Checks if file exists AND if it is an image
     * @param location Directory of file
     * @return is file valid
     */
    private boolean checkFile(String location) {
        File f = new File(location);

        if(!f.exists())
            return false;

        //String mimetype = new MimetypesFileTypeMap().getContentType(f);
        // String type = mimetype.split("/")[0];
        // if(!type.equals("image"))
        //     return false;

        return true;
    }

    /**
     * Gets the last modified date of a file
     * @param location Directory of file
     * @return Calendar object
     */
    private Calendar getFileDate(String location) {
        Calendar date = Calendar.getInstance();

        File file = new File(location);
        date.setTimeInMillis(file.lastModified());
        date.set(Calendar.MILLISECOND, 0);

        return date;
    }

//    public String getDateTaken() {
//        return this.date.getTime().toString();
//    }

    public Calendar getDateTaken() {
        return this.date;
    }

    public int AddTag(String p_name, String p_value) throws InvalidParameterException {
        String name = p_name.trim();
        String value = p_value.trim();

        if(name.length() == 0 || value.length() == 0)
            return 4;

        // Disallow multiple location tags
        if(name.equals("Location") || name.equals("location")) {
            if(tags.containsKey("location"))
                if(tags.get("location").size() > 0)
                    return 1;
            if(tags.containsKey("Location"))
                if(tags.get("Location").size() > 0)
                    return 1;
        }

        // Disallow duplicate tag values
        if(tags.containsKey(name))
            if(tags.get(name).contains(value))
                return 2;

        ArrayList<String> values;
        if(tags.containsKey(name))
            values = tags.get(name);
        else {
            values = new ArrayList<>();
            tags.put(name, values);
        }
        values.add(value);
        //System.out.println(tags.get(name));
        return 0;
    }

    public boolean deleteTag(String name, String value) {
        ArrayList<String> vals = tags.get(name);
        if(vals == null)
            return false;
        if(vals.contains(value)) {
            vals.remove(value);
            return true;
        }
        return false;
    }

    public boolean deleteTag(String name) {
        ArrayList<String> vals = tags.get(name);
        if(vals == null)
            return false;
        tags.remove("Location");
        return true;
    }

    public boolean checkTag(String name, String value) {
        ArrayList<String> vals = tags.get(name);
        if(vals == null) {
            return false;
        }
        if(vals.contains(value)) {
            return true;
        }
        return false;
    }

    public boolean checkTagCompletion(String name, String value){
        System.out.println(this.getCaption());
        if(value.isEmpty()){
            return true;
        }
        ArrayList<String> vals = tags.get(name);
        if(vals == null) {
            return false;
        }
        for(String s : vals){
            System.out.println(s);
        }
        return vals.stream()
                .map(s -> s.toUpperCase())
                .anyMatch(s -> s.startsWith(value.toUpperCase()));
    }

    public HashMap<String, ArrayList<String>> getTags() {
        return tags;
    }

    public ArrayList<String> getTags(String name){
        ArrayList<String> vals = tags.get(name);
        if(vals == null) {
            return new ArrayList<String>();
        }
        return vals;
    }

    /**
     * For development purposes
     * @return
     */
    @Override
    public String toString() {
        return "Caption: " + caption + "\tLocation: " + location + "\tDate: " + date.getTime();
    }

    public Photo clone() {
        Photo clone;
        try {
            clone = new Photo(this.location);
        } catch (FileNotFoundException e) {
            //Alerts.showError("Error Making Copy of Photo", e.getStackTrace().toString());
            return null;
        }
        HashMap<String, ArrayList<String>> cloneTags = new HashMap<String, ArrayList<String>>();
        for(String tagName: this.tags.keySet()) {
            cloneTags.put(tagName, (ArrayList<String>) this.tags.get(tagName).clone());
        }
        clone.tags = cloneTags;
        clone.caption = this.caption;
        clone.date = (Calendar) this.date.clone();
        return clone;
    }

    /**
     * Returns an ImageView object of image
     * @param photoSize
     * @return
     */
    public ImageView getImageView(Context context, int photoSize) {
        Bitmap bmp = getBitmapFromUri(context, Uri.parse(location));
        float ratio = (float)bmp.getWidth() / (float)bmp.getHeight();
        Bitmap scaled = Bitmap.createScaledBitmap(bmp, (int)(photoSize*ratio), photoSize, true);
        ImageView imgView = new ImageView(context);
        imgView.setMaxHeight(photoSize);
        imgView.setMinimumHeight(photoSize);

        //File file = new File(photos.get(position).getLocation());
        //imgView.setImageURI(Uri.fromFile(file));
        imgView.setImageBitmap(scaled);

        return imgView;
    }

    // TODO
    /**
     * Returns Image object for photo
     * @return Image object
     */
    /*public Image getImage() {
        if(new File(location).exists())
            return new Image("File:"+location);
        else {
            System.out.println("Image not found");
            return new Image("File:data/missing.png");
        }
    }*/

    public static void writePhoto(Photo p, String dir, String fileName) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(dir + File.separator + fileName)
        );
        oos.writeObject(p);
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLocation() {
        if(location != null)
            return location;
        else
            return "";
    }

    private Bitmap getBitmapFromUri(Context context, Uri uri) {
        ParcelFileDescriptor parcelFileDescriptor =
                null;
        try {
            parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        try {
            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }
}

