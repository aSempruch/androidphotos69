package group69.photos69.model;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Album object
 * @author Alan
 */
public class Album implements Serializable {
    String name;
    User user;
    Calendar earliestDate;
    Calendar latestDate;
    List<Photo> photoList;

    public Album(String name, List<Photo> photoList, User user) {
        this.name = name;
        this.photoList = photoList;
        this.user = user;
        if(!photoList.isEmpty()) {
            this.earliestDate = photoList.get(0).getDateTaken();
            this.latestDate = photoList.get(0).getDateTaken();
            for(Photo p : photoList) {
                handleAlbumDate(p);
            }
        }
    }

    public Album(String name, User user) {
        this(name, new ArrayList<Photo>(), user);
    }

    /**
     * Adds photo to album
     * @param location Location of photo
     * @return true/false operation successful
     */
    public boolean addPhoto(String location) {
        Photo newPhoto;
        //user.albumList.remove(this);
        try {
            newPhoto = new Photo(location);
            photoList.add(newPhoto);
        } catch (FileNotFoundException e) {
            System.out.println("Unable to import photo. File not found : " + location);
            return false;
        }
        //user.albumList.add(this);
        handleAlbumDate(newPhoto);

        /*try {
            User.writeObject(user, "data/users", user.toString());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
        return true;
    }

    public boolean addPhoto(Photo p) {
        photoList.add(p);
        handleAlbumDate(p);
        return true;
    }

    public boolean deletePhoto(Photo p) {
        if(photoList.contains(p)){
            photoList.remove(p);
            return true;
        }
        return false;
    }

    /**
     * Getter for list of photos in album
     * @return List of photos
     */
    public List<Photo> getPhotos() {
        return photoList;
    }

    public int getPhotoCount() {
        return photoList.size();
    }

    public Calendar getEarliest() {
        return earliestDate;
    }

    public Calendar getLatest() {
        return latestDate;
    }

    /**
     * Getter for album name
     * @return album name
     */
    public String getName() {
        return name;
    }

    public User getUser() {
        return user;
    }

    public void rename(String newName) {
        name = newName;
    }

    public void replaceList(List<Photo> pList){
        this.photoList = pList;
    }

    private void handleAlbumDate(Photo p) {
        if(this.earliestDate == null) {
            earliestDate = p.getDateTaken();
        }
        else if(earliestDate.after(p.getDateTaken())) {
            earliestDate = p.getDateTaken();
        }

        if(latestDate == null) {
            latestDate = p.getDateTaken();
        }
        else if(latestDate.before(p.getDateTaken())) {
            latestDate = p.getDateTaken();
        }
    }

    public static Album readObject(String dir, String filename) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(dir + File.separator + filename)
        );
        Album album = (Album)ois.readObject();
        ois.close();
        return album;
    }

    public static void writeObject(Album album, String dir, String fileName) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(dir + File.separator + fileName)
        );
        oos.writeObject(album);
        oos.close();
    }
}

