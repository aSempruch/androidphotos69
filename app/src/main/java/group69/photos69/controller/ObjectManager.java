package group69.photos69.controller;

import android.app.Activity;
import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import group69.photos69.model.Album;
import group69.photos69.model.Photo;
import group69.photos69.model.User;

public class ObjectManager {

    private static ObjectManager instance;
    Context ctx;
    List<Album> albums;

    private ObjectManager(Context ctx) {
        this.ctx = ctx;
        this.albums = loadOrMakeSerializedList();
    }

    public static ObjectManager init(Context ctx) {
        if(instance == null)
            instance = new ObjectManager(ctx);
        return instance;
    }

    public static ObjectManager getInstance() {
        return instance;
    }

    public Album getIndex(int index) {
        return albums.get(index);
    }

    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * Checks to see if Serialized List albums.list already exists.
     * If it does, return the serialized list.
     * Else, it creates a new album list, serializes it and returns that.
     * @return
     */
    public List<Album> loadOrMakeSerializedList(){
        System.out.println(ctx);
        File file = new File(ctx.getExternalFilesDir(null), "albums.list");
        if(file.exists()){
            System.out.println("Squid: Found serialized list reading that");
            try {
                return readAlbumList();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Squid: Didn't find serialized list, making new one");
//        List<Photo> tempPhotos = new ArrayList<Photo>();
        List<Album> tempAlbums = new ArrayList<Album>();
//        Album album = new Album("Default", tempPhotos, new User("MainUser"));
//
//        tempAlbums.add(album);
//
        try {
            this.writeAlbumList(tempAlbums);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tempAlbums;
    }

    public List<Album> readAlbumList() throws IOException, ClassNotFoundException {
        File file = new File(ctx.getExternalFilesDir(null), "albums.list");
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(file)
        );
        List<Album> albumList = (List<Album>)ois.readObject();
        ois.close();
        System.out.println("squid: Successfully read serialized albumlist");
        return albumList;
    }

    public void writeAlbumList(List<Album> list) throws IOException {
        File file = new File(ctx.getExternalFilesDir(null), "albums.list");
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(file)
        );
        oos.writeObject(list);
        oos.close();
        System.out.println("squid: Successfully wrote serialized albumlist");
    }

    public void writeAlbumList() {
        File file = new File(ctx.getExternalFilesDir(null), "albums.list");
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(
                    new FileOutputStream(file)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            oos.writeObject(this.albums);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("squid: Successfully wrote serialized albumlist");
    }
}
