package group69.photos69.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;

import group69.photos69.R;
import group69.photos69.model.Album;
import group69.photos69.model.Photo;
import group69.photos69.util.Prompts;

public class SearchPhotoView extends Activity {

    Photo photo;
    Album album;
    //LinearLayout main;
    LinearLayout imageView;
    TextView text_location;
    ListView peopleList;
    ArrayList<String> peopleTagList;
    ArrayAdapter<String> peopleAdapter;
    int PhotoIndex;
    String locationQuery;
    String peopleQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);

        this.imageView = findViewById(R.id.ImageView_img);
        this.text_location = findViewById(R.id.Text_Location);
        this.peopleList = findViewById(R.id.List_People);

        Intent intent = getIntent();
        this.photo = (Photo) intent.getSerializableExtra("PhotoClicked");
        this.album = (Album) intent.getSerializableExtra("SearchAlbum");
        this.peopleTagList = photo.getTags("People");
        this.PhotoIndex = intent.getIntExtra("PhotoIndex", -1);
        this.locationQuery = intent.getStringExtra("locationQuery");
        this.peopleQuery = intent.getStringExtra("peopleQuery");

        Button peopleButton = (Button) findViewById(R.id.Button_People);
        peopleButton.setVisibility(View.GONE);
        Button locationButton = (Button) findViewById(R.id.Button_Location);
        locationButton.setVisibility(View.GONE);

        /* Set ActionBar Title */
        getActionBar().setTitle(this.photo.getCaption());

        /* Display Photo in ImageView element */
        this.imageView.addView(photo.getImageView(this, 500));

        /* Attaches List View to */
        peopleAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                peopleTagList);

        peopleList.setAdapter(peopleAdapter);

//        peopleList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                PopupMenu menu = new PopupMenu(SearchPhotoView.this, view);
//                MenuInflater inflater = menu.getMenuInflater();
//                inflater.inflate(R.menu.people_menu, menu.getMenu());
//                final String pTag = peopleTagList.get(position);
//
//                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        switch(item.getItemId()) {
//                            case R.id.PTagMenu_Delete:
//                                photo.deleteTag("People", pTag);
//                                loadTags();
//                                ObjectManager.getInstance().writeAlbumList();
//                                break;
//                        }
//                        peopleAdapter.notifyDataSetChanged();
//                        return true;
//                    }
//                });
//
//                menu.show();
//                return true;
//            }
//        });

        /* Set Location Value, People Tags */
        loadTags();
    }

    private void loadTags() {
        HashMap<String, ArrayList<String>> tags = photo.getTags();

        // TODO : Remove
        System.out.println(tags);

        // If location tag exists
        if(tags.containsKey("Location") && tags.get("Location").size() > 0) {
            text_location.setText(tags.get("Location").get(0));
        }
        peopleTagList = tags.get("People");
        peopleAdapter.notifyDataSetChanged();
    }

    public void onEditLocationButtonPress(View v) {
        Prompts.getInput(this, "New Location", new Consumer<String>() {
            @Override
            public void accept(String s) {
                photo.deleteTag("Location");
                photo.AddTag("Location", s);
                ObjectManager.getInstance().writeAlbumList();
                loadTags();
            }
        });
    }

    public void onNextButtonClick(View v){
        Intent intent = new Intent(SearchPhotoView.this, SearchPhotoView.class);
        intent.putExtra("SearchAlbum", album);
        intent.putExtra("LocationQuery",locationQuery);
        intent.putExtra("PeopleQuery", peopleQuery);
        if(PhotoIndex == album.getPhotoCount() - 1) {
            intent.putExtra("PhotoClicked", album.getPhotos().get(0));
            intent.putExtra("PhotoIndex", 0);
        }
        else{
            intent.putExtra("PhotoClicked", album.getPhotos().get(PhotoIndex + 1));
            intent.putExtra("PhotoIndex", PhotoIndex + 1);
        }
        startActivity(intent);
    }

    public void onPrevButtonClick(View v){
        Intent intent = new Intent(SearchPhotoView.this, SearchPhotoView.class);
        intent.putExtra("SearchAlbum", album);
        intent.putExtra("LocationQuery",locationQuery);
        intent.putExtra("PeopleQuery", peopleQuery);
        if(PhotoIndex == 0){
            intent.putExtra("PhotoClicked", album.getPhotos().get(album.getPhotoCount() - 1));
            intent.putExtra("PhotoIndex", album.getPhotoCount() - 1);
        }
        else {
            intent.putExtra("PhotoClicked", album.getPhotos().get(PhotoIndex - 1));
            intent.putExtra("PhotoIndex", PhotoIndex - 1);
        }
        startActivity(intent);
    }

    public void onAddPeopleButtonPress(View v) {
        Prompts.getInput(this, "New Person Tag", s -> {
            if(s.trim().length() == 0){
                Prompts.showError(SearchPhotoView.this,"Illegal Tag Name", "Tag cannot be spaces or empty!");
                return;
            }
            if(photo.getTags("People").contains(s.trim())){
                Prompts.showError(SearchPhotoView.this,"Duplicate Tag Name", "Tag " + s.trim() + " already exists!");
                return;
            }
            photo.AddTag("People", s.trim());
            ObjectManager.getInstance().writeAlbumList();
            loadTags();
        });
    }

    public void onBackPressed(){
        Intent intent = new Intent(SearchPhotoView.this, SearchView.class);
        System.out.println(locationQuery);
        System.out.println(peopleQuery);
        intent.putExtra("locationQuery", locationQuery);
        intent.putExtra("peopleQuery", peopleQuery);
        startActivity(intent);
    }
}
