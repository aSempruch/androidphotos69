package group69.photos69.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.PopupMenu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.util.List;

import group69.photos69.R;
import group69.photos69.model.Album;
import group69.photos69.model.Photo;

public class AlbumView extends Activity {

    Album album;
    GridView PhotoList;
    List<Album> albumList;
    int albumIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_view);
        this.PhotoList = (GridView) findViewById(R.id.PhotoList);

        Intent intent = getIntent();
        albumIndex = intent.getIntExtra("AlbumClicked", -1);
        if(albumIndex < 0)
            throw new Error("Unable to retrieve album index from intent");

        this.album = ObjectManager.getInstance().getIndex(albumIndex);
        this.albumList = ObjectManager.getInstance().getAlbums();

        /* Set Action Bar Title */
        getActionBar().setTitle(album.getName());

        PhotoList.setAdapter(new ImageAdapter(this, album.getPhotos(), 200));

        /* Set Photo Click Listener */
        PhotoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Photo p = (Photo) parent.getAdapter().getItem(position);
                Log.d("AlbumView", "Launching Photo View activity due to photo tap");
                Intent intent = new Intent(AlbumView.this, PhotoView.class);
                intent.putExtra("AlbumClicked", albumIndex);
                intent.putExtra("PhotoClicked", ObjectManager.getInstance().getIndex(albumIndex).getPhotos().indexOf(p));
                startActivity(intent);
            }
        });

        /* Set Photo LongClick Listener */
        PhotoList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                PopupMenu menu = new PopupMenu(AlbumView.this, view);
                MenuInflater inflater = menu.getMenuInflater();
                inflater.inflate(R.menu.photo_menu, menu.getMenu());
                final Photo photo = album.getPhotos().get(position);

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()) {
                            case R.id.PMenu_Delete:
                                album.deletePhoto(photo);
                                ObjectManager.getInstance().writeAlbumList();
                                break;
                            case R.id.PMenu_Copy:
                                Intent intent = new Intent(AlbumView.this, CopyView.class);
                                intent.putExtra("PhotoClicked", ObjectManager.getInstance().getIndex(albumIndex).getPhotos().indexOf(photo));
                                intent.putExtra("AlbumClicked", albumIndex);
                                startActivityForResult(intent, 420);
                                break;
                            case R.id.PMenu_Move:
                                intent = new Intent(AlbumView.this, CopyView.class);
                                intent.putExtra("PhotoClicked", ObjectManager.getInstance().getIndex(albumIndex).getPhotos().indexOf(photo));
                                intent.putExtra("AlbumClicked", albumIndex);
                                startActivityForResult(intent, 400);
                                break;
                        }
                        ((BaseAdapter)(PhotoList.getAdapter())).notifyDataSetChanged();
                        return true;
                    }
                });

                menu.show();
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri = null;
            if (data != null) {

                uri = data.getData();

                /*uri = data.getData();
                albumList.remove(album);

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                String realPath = "";

                if ("primary".equalsIgnoreCase(type)) {
                    realPath =  Environment.getExternalStorageDirectory() + "/" + split[1];
                }*/
                //Log.i("Image Imported", "Uri: " + uri.getEncodedPath() + " Real Path: " + realPath);
                album.addPhoto(uri.toString());
                ((BaseAdapter)(PhotoList.getAdapter())).notifyDataSetChanged();
                System.out.println(PhotoList.getAdapter().getCount());
                ObjectManager.getInstance().writeAlbumList();

                /*albumList.add(album);*/
//                try {
//                    new AlbumList().writeAlbumList(albumList);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
        }
        if(requestCode == 420 && resultCode == Activity.RESULT_OK){
            System.out.println("squid: copying underway....");
            int AlbumIndex = data.getIntExtra("AlbumClicked", -1);
            int PhotoIndex = data.getIntExtra("PhotoClicked", -1);
            System.out.println(AlbumIndex + " " + PhotoIndex);
            if(AlbumIndex < 0 || PhotoIndex < 0){
                System.out.println("Album or Photo Index not passed successfully");
                return;
            }
            Album a = ObjectManager.getInstance().getIndex(AlbumIndex);
            a.addPhoto(this.album.getPhotos().get(PhotoIndex).clone());
            ((BaseAdapter)(PhotoList.getAdapter())).notifyDataSetChanged();
            System.out.println(PhotoList.getAdapter().getCount());
            ObjectManager.getInstance().writeAlbumList();
        }
        if(requestCode == 400 && resultCode == Activity.RESULT_OK){
            System.out.println("squid: copying underway....");
            int AlbumIndex = data.getIntExtra("AlbumClicked", -1);
            int PhotoIndex = data.getIntExtra("PhotoClicked", -1);
            System.out.println(AlbumIndex + " " + PhotoIndex);
            if(AlbumIndex < 0 || PhotoIndex < 0){
                System.out.println("Album or Photo Index not passed successfully");
                return;
            }
            Album a = ObjectManager.getInstance().getIndex(AlbumIndex);
            Photo toMove = this.album.getPhotos().get(PhotoIndex);
            a.addPhoto(toMove.clone());
            this.album.deletePhoto(toMove);
            ((BaseAdapter)(PhotoList.getAdapter())).notifyDataSetChanged();
            System.out.println(PhotoList.getAdapter().getCount());
            ObjectManager.getInstance().writeAlbumList();
        }
    }

    public void onAddPhotoButtonClick(View v) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, 10);
    }

    public void onBackPressed(){
        Intent intent = new Intent(AlbumView.this, AlbumList.class);
        startActivity(intent);
    }
}
