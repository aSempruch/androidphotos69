package group69.photos69.controller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.PopupMenu;

import java.util.ArrayList;
import java.util.List;

import group69.photos69.R;
import group69.photos69.model.Album;
import group69.photos69.model.Photo;
import group69.photos69.model.User;

public class SearchView extends Activity {

    Album album;
    List<Photo> photoList;
    GridView SearchList;
    List<Album> albumList;
    EditText locationSearch;
    EditText peopleSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);

        this.SearchList = (GridView) findViewById(R.id.SearchList);


        this.albumList = ObjectManager.getInstance().getAlbums();
        this.locationSearch = findViewById(R.id.Search_Location);
        this.peopleSearch = findViewById(R.id.Search_Name);

        //this.album = new Album("",new ArrayList<Photo>(), new User(""));
        //this.album = albumList.get(0);
        photoList = new ArrayList<Photo>();
        album = new Album("SearchResults", photoList, new User("search"));

        /* Set Action Bar Title */
        getActionBar().setTitle("Search");

        SearchList.setAdapter(new ImageAdapter(this, photoList, 200));

        locationSearch.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count)
            {
                photoList.clear();
                photoList.addAll(searchAlbumList(s.toString(), peopleSearch.getText().toString()));
                ((BaseAdapter)(SearchList.getAdapter())).notifyDataSetChanged();
                album = new Album("SearchResults", photoList, new User("search"));
            }
        });

        peopleSearch.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count)
            {
                photoList.clear();
                photoList.addAll(searchAlbumList(locationSearch.getText().toString(), s.toString()));
                ((BaseAdapter)(SearchList.getAdapter())).notifyDataSetChanged();
                album = new Album("SearchResults", photoList, new User("search"));
            }
        });

        /* Set Photo Click Listener */
        SearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Photo p = (Photo) parent.getAdapter().getItem(position);
                Log.d("SearchView", "Launching Photo View activity due to photo tap");
                Intent intent = new Intent(SearchView.this, SearchPhotoView.class);
                intent.putExtra("SearchAlbum", album);
                intent.putExtra("PhotoIndex", album.getPhotos().indexOf(p));
                intent.putExtra("PhotoClicked", p);
                intent.putExtra("locationQuery",locationSearch.getText().toString());
                intent.putExtra("peopleQuery", peopleSearch.getText().toString());
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        System.out.println(intent.getStringExtra("locationQuery"));
        System.out.println(intent.getStringExtra("peopleQuery"));
        if(intent.hasExtra("locationQuery") && intent.hasExtra("peopleQuery")){
            locationSearch.setText(intent.getStringExtra("locationQuery"));
            peopleSearch.setText(intent.getStringExtra("peopleQuery"));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri = null;
            if (data != null) {

                uri = data.getData();

                /*uri = data.getData();
                albumList.remove(album);

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                String realPath = "";

                if ("primary".equalsIgnoreCase(type)) {
                    realPath =  Environment.getExternalStorageDirectory() + "/" + split[1];
                }*/
                //Log.i("Image Imported", "Uri: " + uri.getEncodedPath() + " Real Path: " + realPath);
                album.addPhoto(uri.toString());
                ((BaseAdapter)(SearchList.getAdapter())).notifyDataSetChanged();
                System.out.println(SearchList.getAdapter().getCount());
            }
        }
        if(requestCode == 420 && resultCode == Activity.RESULT_OK){
            System.out.println("squid: copying underway....");
            int AlbumIndex = data.getIntExtra("AlbumClicked", -1);
            int PhotoIndex = data.getIntExtra("PhotoClicked", -1);
            System.out.println(AlbumIndex + " " + PhotoIndex);
            if(AlbumIndex < 0 || PhotoIndex < 0){
                System.out.println("Album or Photo Index not passed successfully");
                return;
            }
            Album a = ObjectManager.getInstance().getIndex(AlbumIndex);
            a.addPhoto(this.album.getPhotos().get(PhotoIndex).clone());
            ((BaseAdapter)(SearchList.getAdapter())).notifyDataSetChanged();
            System.out.println(SearchList.getAdapter().getCount());
            ObjectManager.getInstance().writeAlbumList();
        }
        if(requestCode == 400 && resultCode == Activity.RESULT_OK){
            System.out.println("squid: copying underway....");
            int AlbumIndex = data.getIntExtra("AlbumClicked", -1);
            int PhotoIndex = data.getIntExtra("PhotoClicked", -1);
            System.out.println(AlbumIndex + " " + PhotoIndex);
            if(AlbumIndex < 0 || PhotoIndex < 0){
                System.out.println("Album or Photo Index not passed successfully");
                return;
            }
            Album a = ObjectManager.getInstance().getIndex(AlbumIndex);
            Photo toMove = this.album.getPhotos().get(PhotoIndex);
            a.addPhoto(toMove.clone());
            this.album.deletePhoto(toMove);
            ((BaseAdapter)(SearchList.getAdapter())).notifyDataSetChanged();
            System.out.println(SearchList.getAdapter().getCount());
            ObjectManager.getInstance().writeAlbumList();
        }
    }

    public void onBackPressed(){
        Intent intent = new Intent(SearchView.this, AlbumList.class);
        startActivity(intent);
    }

    private List<Photo> searchAlbumList(String location, String people){
        ArrayList<Photo> results = new ArrayList<Photo>();
        if(location.isEmpty() && people.isEmpty()){
            return results;
        }
        for(Album a : albumList) {
            for (Photo p : a.getPhotos()) {
                if (p.checkTagCompletion("Location", location) && p.checkTagCompletion("People", people)) {
                    results.add(p);
                    System.out.println(p.getCaption() + " added.");
                } else {
                    System.out.println(p.getCaption() + " not added.");
                }
            }
        }
        return results;
    }
}
