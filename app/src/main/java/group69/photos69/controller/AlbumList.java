package group69.photos69.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import group69.photos69.R;
import group69.photos69.model.Album;
import group69.photos69.model.Photo;
import group69.photos69.model.User;
import group69.photos69.util.Prompts;

import static group69.photos69.util.Prompts.getInput;

public class AlbumList extends Activity {
    List<Album> initialList;
    ListView AlbumList;

    @Override
    public void setFinishOnTouchOutside(boolean finish) {
        super.setFinishOnTouchOutside(finish);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialList = ObjectManager.init(this).albums;

        setContentView(R.layout.activity_albumlist_view);

        /* Set Action Bar Title */
        getActionBar().setTitle("Album Viewer");

        AlbumList = (ListView) findViewById(R.id.AlbumList);

        AlbumList.setAdapter(new AlbumlistAdapter(this, initialList));

        /* Set Album Click Listener */
        AlbumList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Album a = (Album) parent.getAdapter().getItem(position);
                Log.d("AlbumList", "Launching AlbumView activity due to album tap");
                Intent intent = new Intent(AlbumList.this, AlbumView.class);
                intent.putExtra("AlbumClicked", initialList.indexOf(a));
                startActivity(intent);
            }
        });

        /* Set Album LongClick Listener */
        AlbumList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                PopupMenu menu = new PopupMenu(AlbumList.this, view);
                MenuInflater inflater = menu.getMenuInflater();
                inflater.inflate(R.menu.album_menu, menu.getMenu());
                final Album album = (Album) AlbumList.getItemAtPosition(position);

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()) {
                            case R.id.AMenu_Delete:
                                initialList.remove(album);
                                try {
                                    ObjectManager.getInstance().writeAlbumList(initialList);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case R.id.AMenu_Rename:
                                getInput(AlbumList.this, "Rename to:", new Consumer<String>() {
                                    @Override
                                    public void accept(String s) {
                                        for(Album a : initialList){
                                            System.out.println("squid: " + a.getName() + " vs " + s);
                                            if(a.getName().equals(s)){
                                                Prompts.showError(AlbumList.this,"Duplicate Album Name", "Album named " + s + " already exists!");
                                                return;
                                            }
                                        }
                                        album.rename(s);
                                        try {
                                            ObjectManager.getInstance().writeAlbumList(initialList);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        }

                        ((BaseAdapter)(AlbumList.getAdapter())).notifyDataSetChanged();
                        return true;
                    }
                });

                menu.show();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.action_menu, menu);
        return true;
    }

    public void onSearchButtonClick(MenuItem menuItem){
        Intent intent = new Intent(AlbumList.this, SearchView.class);
        startActivity(intent);
    }

    public void onAddAlbumButtonClick(View v){
        getInput(this, "Album Name:", new Consumer<String>() {
            @Override
            public void accept(String s) {
                if(s.trim().length() == 0){
                    Prompts.showError(AlbumList.this,"Illegal Album Name", "Albums cannot be spaces or empty!");
                    return;
                }
                for(Album a : initialList){
                    System.out.println("squid: " + a.getName() + " vs " + s);
                    if(a.getName().equals(s.trim())){
                        Prompts.showError(AlbumList.this,"Duplicate Album Name", "Album named " + s + " already exists!");
                        return;
                    }
                }
                Album album = new Album(s.trim(), new ArrayList<Photo>(), new User("MainUser"));
                initialList.add(album);
                try {
                    ObjectManager.getInstance().writeAlbumList(initialList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ((BaseAdapter)(AlbumList.getAdapter())).notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private class AlbumlistAdapter extends BaseAdapter{

        Context context;
        List<Album> albumList;

        public AlbumlistAdapter(Context context, List<Album> albumList){
            this.context = context;
            this.albumList = albumList;
        }

        @Override
        public int getCount() {
            return albumList.size();
        }

        @Override
        public Object getItem(int position) {
            return albumList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return albumList.get(position).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setPadding(5, 10, 5, 10);

            Album album = albumList.get(position);


            TextView caption = new TextView(context);
            caption.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            caption.setText(album.getName());
            layout.addView(caption);

            return layout;
        }
    }
}
