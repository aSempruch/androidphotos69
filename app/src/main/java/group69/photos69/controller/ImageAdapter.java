package group69.photos69.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import group69.photos69.model.Photo;

public class ImageAdapter extends BaseAdapter {

    Context context;
    List<Photo> photos;
    int photoSize;

    public ImageAdapter(Context context, List<Photo> photos, int photoSize) {
        this.context = context;
        this.photos = photos;
        this.photoSize = photoSize;
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Object getItem(int position) {
        return photos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return photos.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(5, 10, 5, 10);

        Photo photo = photos.get(position);

        layout.addView(photo.getImageView(context, photoSize));

        /*TextView caption = new TextView(context);
        caption.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        caption.setText(photo.getCaption());
        layout.addView(caption);*/

        return layout;
    }
}
