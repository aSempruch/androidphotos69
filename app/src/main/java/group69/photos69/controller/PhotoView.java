package group69.photos69.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import group69.photos69.util.Prompts;

import group69.photos69.R;
import group69.photos69.model.Photo;

public class PhotoView extends Activity {

    Photo photo;
    //LinearLayout main;
    LinearLayout imageView;
    TextView text_location;
    ListView peopleList;
    ArrayList<String> peopleTagList;
    ArrayAdapter<String> peopleAdapter;
    int AlbumIndex;
    int PhotoIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);

        this.imageView = findViewById(R.id.ImageView_img);
        this.text_location = findViewById(R.id.Text_Location);
        this.peopleList = findViewById(R.id.List_People);

        Intent intent = getIntent();
        PhotoIndex = intent.getIntExtra("PhotoClicked", -1);
        AlbumIndex = intent.getIntExtra("AlbumClicked", -1);
        if(PhotoIndex < 0 && AlbumIndex < 0)
            throw new Error("Unable to retrieve album index from intent");
        this.photo = ObjectManager.getInstance().getIndex(AlbumIndex).getPhotos().get(PhotoIndex);
        this.peopleTagList = photo.getTags("People");

        /* Set ActionBar Title */
        getActionBar().setTitle(this.photo.getCaption());

        /* Display Photo in ImageView element */
        this.imageView.addView(photo.getImageView(this, 500));

        /* Attaches List View to */
        peopleAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                peopleTagList);

        peopleList.setAdapter(peopleAdapter);

        peopleList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                PopupMenu menu = new PopupMenu(PhotoView.this, view);
                MenuInflater inflater = menu.getMenuInflater();
                inflater.inflate(R.menu.people_menu, menu.getMenu());
                final String pTag = peopleTagList.get(position);

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()) {
                            case R.id.PTagMenu_Delete:
                                photo.deleteTag("People", pTag);
                                loadTags();
                                ObjectManager.getInstance().writeAlbumList();
                                break;
                        }
                        peopleAdapter.notifyDataSetChanged();
                        return true;
                    }
                });

                menu.show();
                return true;
            }
        });

        /* Set Location Value, People Tags */
        loadTags();
    }

    private void loadTags() {
        HashMap<String, ArrayList<String>> tags = photo.getTags();

        // TODO : Remove
        System.out.println(tags);

        // If location tag exists
        if(tags.containsKey("Location") && tags.get("Location").size() > 0) {
            text_location.setText(tags.get("Location").get(0));
        }
        peopleTagList = tags.get("People");
        peopleAdapter.notifyDataSetChanged();
    }

    public void onEditLocationButtonPress(View v) {
        Prompts.getInput(this, "New Location", new Consumer<String>() {
            @Override
            public void accept(String s) {
                photo.deleteTag("Location");
                photo.AddTag("Location", s);
                ObjectManager.getInstance().writeAlbumList();
                loadTags();
            }
        });
    }

    public void onNextButtonClick(View v){
        Intent intent = new Intent(PhotoView.this, PhotoView.class);
        intent.putExtra("AlbumClicked", AlbumIndex);
        if(PhotoIndex == ObjectManager.getInstance().getIndex(AlbumIndex).getPhotoCount() - 1) {
            intent.putExtra("PhotoClicked", 0);
        }
        else{
            intent.putExtra("PhotoClicked", PhotoIndex + 1);
        }
        startActivity(intent);
    }

    public void onPrevButtonClick(View v){
        Intent intent = new Intent(PhotoView.this, PhotoView.class);
        intent.putExtra("AlbumClicked", AlbumIndex);
        if(PhotoIndex == 0){
            intent.putExtra("PhotoClicked", ObjectManager.getInstance().getIndex(AlbumIndex).getPhotoCount() - 1);
        }
        else {
            intent.putExtra("PhotoClicked", PhotoIndex - 1);
        }
        startActivity(intent);
    }

    public void onAddPeopleButtonPress(View v) {
        Prompts.getInput(this, "New Person Tag", s -> {
            if(s.trim().length() == 0){
                Prompts.showError(PhotoView.this,"Illegal Tag Name", "Tag cannot be spaces or empty!");
                return;
            }
            if(photo.getTags("People").contains(s.trim())){
                Prompts.showError(PhotoView.this,"Duplicate Tag Name", "Tag " + s.trim() + " already exists!");
                return;
            }
            photo.AddTag("People", s.trim());
            ObjectManager.getInstance().writeAlbumList();
            loadTags();
        });
    }

    public void onBackPressed(){
        Intent intent = new Intent(PhotoView.this, AlbumView.class);
        intent.putExtra("AlbumClicked", AlbumIndex);
        startActivity(intent);
    }
}
