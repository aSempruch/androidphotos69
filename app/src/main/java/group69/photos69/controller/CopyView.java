package group69.photos69.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import group69.photos69.R;
import group69.photos69.model.Album;
import group69.photos69.model.Photo;
import group69.photos69.model.User;
import group69.photos69.util.Prompts;

import static group69.photos69.util.Prompts.getInput;

public class CopyView extends Activity {
    List<Album> initialList;
    ListView AlbumList;
    Photo toCopy;

    @Override
    public void setFinishOnTouchOutside(boolean finish) {
        super.setFinishOnTouchOutside(finish);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialList = ObjectManager.init(this).albums;
        Intent intent = getIntent();
        final int PhotoIndex = intent.getIntExtra("PhotoClicked", -1);
        int AlbumIndex = intent.getIntExtra("AlbumClicked", -1);
        System.out.println(AlbumIndex + " " + PhotoIndex);
        if(PhotoIndex < 0 && AlbumIndex < 0)
            throw new Error("Unable to retrieve album index from intent");
        this.toCopy = ObjectManager.getInstance().getIndex(AlbumIndex).getPhotos().get(PhotoIndex);

        setContentView(R.layout.activity_copymove_view);

        /* Set Action Bar Title */
        getActionBar().setTitle("Copy Image to...");

        AlbumList = (ListView) findViewById(R.id.AlbumList);

        AlbumList.setAdapter(new CopyViewAdapter(this, initialList));

        /* Set Album Click Listener */
        AlbumList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Album a = (Album) parent.getAdapter().getItem(position);
                Intent intent = new Intent(CopyView.this, AlbumView.class);
                intent.putExtra("AlbumClicked", initialList.indexOf(a));
                intent.putExtra("PhotoClicked", PhotoIndex);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }

    private class CopyViewAdapter extends BaseAdapter{

        Context context;
        List<Album> albumList;

        public CopyViewAdapter(Context context, List<Album> albumList){
            this.context = context;
            this.albumList = albumList;
        }

        @Override
        public int getCount() {
            return albumList.size();
        }

        @Override
        public Object getItem(int position) {
            return albumList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return albumList.get(position).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setPadding(5, 10, 5, 10);

            Album album = albumList.get(position);


            TextView caption = new TextView(context);
            caption.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            caption.setText(album.getName());
            layout.addView(caption);

            return layout;
        }
    }
}
